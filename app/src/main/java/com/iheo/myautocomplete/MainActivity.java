package com.iheo.myautocomplete;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends Activity {

    private MyTrie trieNode = new MyTrie();

    private static Button insertButton, searchButton;
    private static EditText eText;
    private static ListView listView;
    private static ProgressBar progressBar;
    private Toast toast;

    boolean showFlag = false;
    ArrayAdapter<String> adapter;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Trie Init
        initTrie();



        // Edit text init
        eText = (EditText) findViewById(R.id.editText);
        listView = (ListView) findViewById(R.id.listview);

        insertButton = (Button) findViewById(R.id.insertbutton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        insertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String prefix = eText.getText().toString().toUpperCase();
                if(trieNode.enroll(prefix) == true)
                    toast.makeText(getBaseContext(), "Successfully inserted!", Toast.LENGTH_SHORT).show();
                else
                    toast.makeText(getBaseContext(), "Failed to insert!", Toast.LENGTH_SHORT).show();
            }
        });

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

        eText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = eText.getText().toString();
                adapter.clear();
                if(text.length() > 1) {
                    showFlag = true;
                    String prefix = eText.getText().toString();
                    List<String> mylist = trieNode.autoComplete(prefix);
                    if (mylist == null) {
                        adapter.add("No word found");
                    } else {
                        adapter.addAll(mylist);
                    }

                }
                else {
                    showFlag = false;
                }
                listView.setAdapter(adapter);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {       }
            public void afterTextChanged(Editable s) {        }
        });
    }

    private void initTrie() {
        new Thread() {
            public void run() {
                try {
                    // code runs in a thread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            int i = 0;
                            try {
                                DataInputStream textFileStream = new DataInputStream(getAssets().open("cmudict.0.7aword.txt"));
                                //DataInputStream textFileStream = new DataInputStream(getAssets().open("cmudict.0.7aword.short.txt")); // for test
                                Scanner inFile = new Scanner(textFileStream);
                                while (inFile.hasNext()) {
                                    trieNode.insert(inFile.next());
                                    progressBar.setProgress((int) (1.0*i++/10000*100));
                                }
                                inFile.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    Thread.sleep(100);

                } catch (final Exception ex) {
                    //Log.i("---", "Exception in thread");
                }
            }
        }.start();

    }
}
