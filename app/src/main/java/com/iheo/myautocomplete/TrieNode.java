package com.iheo.myautocomplete;

import java.util.LinkedList;

public class TrieNode {	
		
	boolean isEndOfWord = false;
	LinkedList<TrieNode> childList;	
	private char ch;	
	
	public TrieNode(char ch) {
		this.ch = ch;
		this.childList = new LinkedList<TrieNode>();	// child list is also a linked list
	}
	
	// find the child node with an element "ch"
	public TrieNode getChildNode(char ch) {		
		if(childList != null) {
			for(TrieNode eachChild : childList) {
				if(eachChild.getElement() == Character.toUpperCase(ch))
					return eachChild;
			}
		}
		return null;
	}
	
	// a simple getter
	public char getElement() {
		return ch;
	}
}
